// run k6 with: k6 run performance-test.js
import http from 'k6/http';
export const options = {
    stages: [
        { duration: "20s", target: 1 },
    ],
    // noConnectionReuse: true,
};
export default function () {
    http.get('http://localhost:80/api', {
        retry: {
            attempts: 3,
            delay: 2 //in seconds
        }
    });
}