# Getting started

This repository is for dockerize `production-ready-demo` application 🚀

## Services

We have 4 services

1. `gateway` nginx load-balancer & `reverse proxy`
2. `web-app` the front-end
3. `server` API
4. `database` a mongodb database

### Running the stack

```bash
# make sure you inside production folder
cd production
# You can copy example.env file to .env
cp example.env .env
# Setup your correct environment variables...
# Make sure you setup your `environment variables` & docker compose installed

# 1- exec `mongo-init.sh` script to setup mongo-init.js
# see https://www.mongodb.com/community/forums/t/credentials-with-docker-compose/255648

chmod +x ./mongo-init.sh
# run script
./mongo-init.sh

# Start the stack by running
docker compose up -d 
# That's it
```

### Testing using grafana/k6

Link: https://grafana.com/docs/k6/latest/get-started/installation/

```bash

# pulling 
docker pull grafana/k6

docker run --rm

```