#!/bin/bash

if [ -f .env ]; then
    while IFS= read -r line; do
        if [[ ! $line =~ ^\s*($|#) ]]; then
            echo "export $line"
            export "$line"
        fi
    done < .env
else
    echo "Error: .env file not found"
    exit 1
fi
# generate mongo-init.js file
(envsubst < ./.mongo/docker-entrypoint-initdb.d/mongo-init.js.template) > ./.mongo/docker-entrypoint-initdb.d/mongo-init.js
