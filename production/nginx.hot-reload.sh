#!/bin/bash
nginx_service_name=gateway
nginx_container_id=$(docker ps -f name=$nginx_service_name -q | tail -n1)
reload_nginx() {  
  docker exec $nginx_container_id /usr/sbin/nginx -s reload
  echo "nginx reloaded" 
}
reload_nginx