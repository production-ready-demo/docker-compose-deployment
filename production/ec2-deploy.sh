#!/bin/bash

zero_downtime_deploy() {  
  service_name=$1
  echo "pulling: $service_name"
  docker-compose pull $service_name

  old_container_id=$(docker ps -f name=$service_name -q | tail -n1)
  # bring a new container online, running new code  
  # (nginx continues routing to the old container only)  
  docker-compose up -d --no-deps --scale $service_name=2 --no-recreate $service_name
  docker ps -f name=$service_name
  # wait for new container to be available  
  new_container_id=$(docker ps -f name=$service_name -q | head -n1)

  while :
    do
        new_container_helth_check=$(docker inspect -f '{{ .State.Health.Status }}' $new_container_id)
  
        if [[ "$new_container_helth_check" == "healthy" ]]; then
            echo "container is healthy now"
            break
        fi

        echo "waiting..."
        sleep 5
    done
  # start routing requests to the new container (as well as the old)  
  sleep 5
  echo "reloading nginx"
  bash ./nginx.hot-reload.sh
  echo "stop & remove old container"
  # take the old container offline  
  docker stop $old_container_id
  docker rm $old_container_id
  
  # 
  docker-compose up -d --no-deps --scale $service_name=1 --no-recreate $service_name
  echo "reload nginx"
  bash ./nginx.hot-reload.sh
}
zero_downtime_deploy "$1"